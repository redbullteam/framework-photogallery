<?php
use Modules\Framework\Globalization\i18n;

/**
 * @var \Modules\Framework\Mvc\View\PhpView $this
 * @var \Modules\Framework\Mvc\View\Helpers\Section $__section
 * @var \Modules\Framework\Utils\Url $__url
 */
$this->extend(Modules\Framework\Core\Config\Config::getInstance()->admin['layout']);
?>
<?php $__section->begin('body'); ?>
<form action="?redirectUrl=!!self" class="form-horizontal" method="post" enctype="multipart/form-data">
	<fieldset>
		<legend>
			<?=$pageTitle?>
		</legend>

		<?=$this->partial("fileUpload", array('fields' => $this->fields, 'parentModel' => 'self', 'parentId' => -1))?>
	</fieldset>
</form>
<?php $__section->end(); ?>