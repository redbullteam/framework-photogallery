<?php
use Modules\Framework\Globalization\i18n;

/**
 * @var \Modules\Framework\Mvc\View\PhpView $this
 * @var \Modules\Framework\Mvc\View\Helpers\Section $__section
 * @var \Modules\Framework\Utils\Url $__url
 */

if (!isset($model) || !$model)
	$model = $__url->params['model'];

if (!isset($fields) || !$fields) {
	$className = \Modules\Framework\Core\Config\Config::getInstance()->mvc['availableModels'][$model];
	$instance = new $className();
	$fields = $instance->getModelMetadata();
}

if (!isset($parentModel) || !$parentModel)
	$parentModel = 'self';

if (!isset($parentId))
	$parentId = -1;

?>
<?php $__section->append('stylesheet'); ?>
	<link rel="stylesheet" href="/Resources/PhotoGallery/css/fileUpload.css"/>
<?php $__section->end(); ?>

<?php $__section->append('javascript'); ?>
	<script src="//yandex.st/angularjs/1.2.16/angular.js"></script>
	<script>!window.angular && document.write(unescape('%3Cscript src="/Resources/PhotoGallery/js/angular/angular.js"%3E%3C/script%3E'))</script>
	<script src="//yandex.st/angularjs/1.2.16/angular-resource.js"></script>
	<script>
		try {
			angular.module("ngResource")
		} catch(err) {
			document.write(unescape('%3Cscript src="/Resources/PhotoGallery/js/angular/angular-resource.js"%3E%3C/script%3E'))
		}
	</script>
	<script src="/Resources/PhotoGallery/js/jquery/jquery-ui-1.10.4.custom.js"></script>
	<script src="/Resources/PhotoGallery/js/angular/directives/oi.file.js"></script>
	<script src="/Resources/PhotoGallery/js/angular/directives/editable.js"></script>
	<script>
		angular.module('admin', ['ngResource', 'oi.file', 'admin.directives'])
			.controller('fileManagerCtrl', function ($scope, $files, $photoGalleryItems, $filter) {
				$scope.files = $files.query();

				var sortableTable = $('#sortable').find('tbody').sortable({
					update: function (e, ui) {
						$scope.$apply(function () {
							var next = ui.item.next(),
								sortOrder = next.length
									? parseInt(next.scope().item.sortOrder)
									: parseInt(ui.item.prev().scope().item.sortOrder) + 10;

							var file = ui.item.scope().item;
							file.sortOrder = sortOrder;

							var galeryItem = new $photoGalleryItems(angular.extend({}, file));
							galeryItem.$save();

							$scope.files
								.filter(function (item) { return item != file && item.sortOrder >= sortOrder; })
								.forEach(function (item, i) {
									item.sortOrder = sortOrder + (i + 1) * 10;
								});
						});
					}
				});

				$scope.uploadoptions = {
					url: '/admin/FileUpload/<?=$model?>/<?=$parentModel?>/<?=$parentId?>',
					fieldName: 'files',
					change: function (file) {
						file.$upload($scope.uploadoptions.url, {})
							.then(function (response) {
								var data = angular.isArray(response.data) ? response.data : [response.data];
								$scope.files = $scope.files.concat(data);
								sortableTable.sortable("refresh");
							});
					}
				};

				$scope.update = function (file, field) {
					return function (value) {
						file[field] = value;
						
						var item = new $photoGalleryItems(angular.extend({}, file));
						item.$save();
					};
				};

				$scope.remove = function (index, file, event) {
					$files.remove({ id: file.id, file: file.filename }, function () {
						$scope.files.splice(index, 1);
					});
					
					sortableTable.sortable("refresh");

					if (event) {
						event.stopPropagation();
						event.preventDefault();
					}
				};
			})

			.service('$files', function ($resource) {
				return $resource('/admin/FileUpload/<?=$model?>/<?=$parentModel?>/:parentId/:id', { parentId: '<?=$parentId?>', id: '@id' });
			})

			.service('$photoGalleryItems', function ($resource) {
				return $resource('/admin/<?=$model?>/:id', { id: '@id' }, {

				});
			});
	</script>
<?php $__section->end(); ?>

<div ng-controller="fileManagerCtrl">
	<input type="file" oi-file="uploadoptions">

	<div class="progress progress-slim" ng-show="progressAll">
		<div class="bar" ng-style="{'width': progressAll+'%'}"></div>
	</div>

	<small class="muted">Для загрузки перетащите файлы на серую область</small>

	<div class="upload-area" oi-file="uploadoptions">
		<table id="sortable" class="table table-striped">
			<thead>
				<tr>
					<th></th>
					<?php foreach ($fields as $fieldName => $field): ?>
						<?php if ($field->hideFromList || ($field->debug == "true" && !$debugEnabled)) continue; ?>
						<th><?=$field->display?></th>
					<?php endforeach; ?>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="item in files | orderBy:sortOrder track by item.id" ng-class="{ success: item.isNew }">
					<td width="20"><i class="icon-resize-vertical"></i></td>
					<?php foreach ($fields as $fieldName => $field): ?>
						<?php if ($field->hideFromList || ($field->debug == "true" && !$debugEnabled)) { continue; } ?>
						<?php switch ($field->uiHint ?: ($field->validation && $field->validation->datatype ? $field->validation->datatype : "text")) {
							case "upload": ?>
								<td width="200">
									<a href="/upload/{{item.<?=$fieldName?>}}" target="_blank"><img ng-src="{{item.thumb}}"></a>
									<div ng-show="item.uploading">
										<div class="progress progress-slim">
											<div class="bar" ng-style="{'width': item.progress+'%'}"></div>
										</div>
									</div>
								</td>
								<?php break; ?>
							<?php default: ?>
								<td>
									<a class="editable" type="text" ng-model="item.<?=$fieldName?>" editable-submit="update(item, '<?=$fieldName?>')" ng-bind="item.<?=$fieldName?>"></a>
								</td>
								<?php break; ?>
						<?php } ?>
					<?php endforeach; ?>
					<td width="120">
						<a class="btn btn-danger" ng-click="remove($index, item, $event)" ng-keydown=""><i class="icon-trash icon-white"></i> <?=i18n::current()->remove;?></a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>