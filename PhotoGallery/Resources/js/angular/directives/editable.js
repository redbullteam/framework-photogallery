angular.module('admin.directives', [])
	.directive('editable', ['$compile', '$parse', function ($compile, $parse) {
		return {
			restrict: 'C',
			require: "ngModel",
			scope: {},
			link: function(scope, element, attrs, ngModel) {
				var template = angular.element('<editable-block/>');

				var format = $parse(attrs.format)(scope);

				if (!angular.isFunction(format))
					format = function (n) { return n; };

				scope.type = attrs.type;
				scope.pattern = new RegExp(attrs.pattern || ".*");

				scope.$watch(attrs.ngModel, function () {
					scope.value = format(ngModel.$viewValue);
					ngModel.$render();
				});

				scope.submit = function (event) {
					if (attrs.editableSubmit) {
						$parse(attrs.editableSubmit)(scope.$parent)(scope.value);
					} else {
						ngModel.$setViewValue(scope.value);
					}

					scope.close(event);
				};

				ngModel.$render = function () {
					if (!ngModel.$viewValue) {
						element
							.html(attrs.editableEmptyText || "Edit")
							.addClass("text-error");
					} else {
						element.removeClass("text-error");
					}
				};

				scope.close = function (event) {
					scope.value = format(ngModel.$viewValue);
					element.next('.editable-block').hide();
					element.show();
					
					if (event) {
						event.stopPropagation();
						event.preventDefault();
					}
				};

				var editor = $compile(template)(scope);

				element.click(function () {
					var el = angular.element(this);

					if (attrs.editableShow) {
						$parse(attrs.editableShow)(scope);
					}

					el.hide();

					var editableBlock = el.next('.editable-block');

					if (editableBlock.length) {
						editableBlock.show();
					} else {
						el.after(editor);
					}
				});
			}
		};
	}])

	.directive('editableBlock', [function () {
		return {
			restrict: 'E',
			template:
				'<div ng-form name="editable" class="editable-block">' +
					'<div class="control-group editable-input" ng-class="{ error: editable.$invalid }" ng-switch on="type">' +
						'<textarea ng-switch-when="textarea" style="width:75%" ng-model="$parent.value" required></textarea>' +
						'<input ng-switch-when="number" type="number" class="input-medium" ng-model="$parent.value" required>' +
						'<div ng-switch-when="currency" class="input-prepend"><span class="add-on">$</span><input type="text" class="input-medium" ng-model="$parent.value" ng-pattern="pattern" required></div>' +
						'<input ng-switch-default type="text" class="input-medium" ng-model="$parent.value" ng-pattern="pattern" required>' +
					'</div> ' +
					'<button class="btn btn-primary" ng-click="submit($event)" ng-disabled="editable.$invalid"><i class="icon-ok icon-white"></i></button> ' +
					'<button class="btn" ng-click="close($event)"><i class="icon-remove"></i></button>' +
				'</div>',
			replace: true
		};
	}]);