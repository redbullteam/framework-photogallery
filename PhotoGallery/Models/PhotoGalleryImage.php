<?php

namespace Modules\PhotoGallery\Models;

use Modules\Framework\Core\Config\Config;
use Modules\Framework\Core\Metadata\MetadataProvider;
use Modules\Framework\Utils\Url;

/**
 * @Display("i18n::gallery")
 * @AdminListView("PhotoGallery:Admin/photoGallery")
 */
class PhotoGalleryImage extends \Modules\Framework\Mvc\Model\BaseModel {
	/**
	 * @Display("i18n::type")
	 * @Required
	 * @UIHint("select", sql = "SELECT type as value, type as display FROM PhotoGalleryImage")
	 * @HideFromList
	 * @Filterable
	 */
	public $type;

	/**
	 * @Display("i18n::parent")
	 * @HideFromList
	 * @Required
	 */
	public $parentId;

	/**
	 * @Display("i18n::file")
	 * @DataType(DataType::UPLOAD)
	 * @Required
	 */
	public $filename;

	/**
	 * @Display("i18n::sortOrder")
	 * @HideFromList
	 */
	public $sortOrder;
	
	protected function beforeSave() {
		if ($this->sortOrder)
			return;

		$sql = sprintf(
			"SELECT MAX(sortOrder) + 10
			FROM `%1\$s%2\$s`
			WHERE `type` = :parentModel AND parentId = :parentId",
			Config::getInstance()->connection['dbPrefix'],
			$this->getName());

		$db = \Modules\Framework\Core\DB::Command()->prepare($sql);

		$db->execute(array(
			"parentModel" => $this->type, 
			"parentId" => (int) $this->parentId));

		$this->sortOrder = $db->fetchColumn() ?: 10;
	}

	protected function afterSave() {
		\Modules\Framework\Core\DB::Command()->exec(sprintf("SET @row := %1\$d;", (int) $this->sortOrder));

		$sql = sprintf(
			"UPDATE `%1\$s%2\$s`
				INNER JOIN (SELECT id, @row := @row + 10 AS row
							FROM `%1\$s%2\$s` tmp
							WHERE tmp.`type` = :innerParentModel AND tmp.parentId = :innerParentId AND tmp.id <> :innerId
							ORDER BY sortOrder) r ON `%1\$s%2\$s`.id = r.id
			SET `%1\$s%2\$s`.sortOrder = r.row
			WHERE `%1\$s%2\$s`.`type` = :parentModel AND `%1\$s%2\$s`.parentId = :parentId AND `%1\$s%2\$s`.sortOrder >= :sortOrder AND `%1\$s%2\$s`.id <> :id",
		               Config::getInstance()->connection['dbPrefix'],
		               $this->getName());

		\Modules\Framework\Core\DB::Command()
			->prepare($sql)
			->execute(array(
				"id" => (int) $this->id,
				"innerId" => (int) $this->id,
				"parentModel" => $this->type,
				"innerParentModel" => $this->type,
				"parentId" => (int) $this->parentId,
				"innerParentId" => (int) $this->parentId,
				"sortOrder" => (int) $this->sortOrder));
	}

	public function mapUploadParams($filename, $post_index) {
		$request = Url::getInstance();

		$this->parentId = (int) $request->params['parentId'];
		$this->type = $request->params['parentModel'];
		$this->filename = $filename;
	}

	function getFileObject() {
		$fileUploadDir = Config::getInstance()->fileUploadDir;
		$fileUploadUrl = Config::getInstance()->fileUploadUrl;
		$file_path = $fileUploadDir . $this->filename;

		$file = clone $this;

		if (is_file($file_path) && $this->filename[0] !== '.') {
			$file->size = filesize($file_path);
			$file->thumb = $fileUploadUrl . '150x150/' . rawurlencode($file->filename);
		}

		return $file;
	}
}